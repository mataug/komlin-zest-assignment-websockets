/**
 * 
 */
package gateway.cassandra;

import java.nio.ByteBuffer;
import java.util.ArrayList;

import org.apache.cassandra.thrift.Cassandra;
import org.apache.cassandra.thrift.Column;
import org.apache.cassandra.thrift.Compression;
import org.apache.cassandra.thrift.ConsistencyLevel;
import org.apache.cassandra.thrift.CqlResult;
import org.apache.cassandra.thrift.CqlRow;
import org.apache.cassandra.thrift.InvalidRequestException;
import org.apache.cassandra.thrift.SchemaDisagreementException;
import org.apache.cassandra.thrift.TimedOutException;
import org.apache.cassandra.thrift.UnavailableException;
import org.apache.thrift.TException;
import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.transport.TFramedTransport;
import org.apache.thrift.transport.TSocket;
import org.apache.thrift.transport.TTransport;
import org.apache.thrift.transport.TTransportException;

/**
 * @author gautam
 *
 */
public class CassandraClient {
	private final TTransport transport;
	private final TProtocol protocol;
	private final Cassandra.Client client;
	public static final String DEFAULT_HOST = "localhost";
	public static final int DEFAULT_PORT = 9160;
	private String keyspace,columnFamily;
	private ConsistencyLevel consistencyLevel= ConsistencyLevel.ONE;
	
	public CassandraClient(String host,int port) throws TTransportException {
		this.transport = new TFramedTransport(new TSocket(host,port));
		this.protocol = new TBinaryProtocol(this.transport);
		this.client = new Cassandra.Client(this.protocol);
		transport.open();
	}
	public CassandraClient() throws TTransportException{
		this(DEFAULT_HOST,DEFAULT_PORT);
	}
	
	public CassandraClient(String keyspace,String columnFamily) throws InvalidRequestException, TException {
		this();
		this.setKeyspace(keyspace);
		this.setColumnFamily(columnFamily);
	}
	public CassandraClient(String keyspace) throws InvalidRequestException, TException {
		this();
		this.setKeyspace(keyspace);
	}
	
	public void close() throws TTransportException{
		transport.flush();
		transport.close();
	}
	
	
	public void cql_create_columnFamily(final String columnFamily) throws InvalidRequestException, UnavailableException, TimedOutException, SchemaDisagreementException, TException {
		final String create_query = String.format(
				"CREATE COLUMNFAMILY %s ( KEY int PRIMARY KEY,VALUE float);",
				columnFamily);
		client.execute_cql_query(ByteBuffer.wrap(create_query.getBytes()), Compression.NONE);
	}
	
	public void cql_insert(final int key,final float value) throws InvalidRequestException, UnavailableException, TimedOutException, SchemaDisagreementException, TException {
		 final String query = "INSERT INTO %s (KEY, VALUE) VALUES (%d, %f);";
		 final String insert_query = String.format(query, columnFamily,key,value);
		 client.execute_cql_query(ByteBuffer.wrap(insert_query.getBytes()), Compression.NONE);		 
	}
	
	public void cql_update (final int key,final float value) throws InvalidRequestException, UnavailableException, TimedOutException, SchemaDisagreementException, TException {
		final String query = "UPDATE %s SET VALUE=%f WHERE KEY=%f;";
		final String update_query = String.format(query, columnFamily,value,key);
		client.execute_cql_query(ByteBuffer.wrap(update_query.getBytes()), Compression.NONE);
	}
	
	public void cql_delete(final int key) throws InvalidRequestException, UnavailableException, TimedOutException, SchemaDisagreementException, TException {
		final String delete_query = String.format("DELETE FROM %s.%s WHERE KEY=%d;", keyspace,columnFamily,key);
		client.execute_cql_query(ByteBuffer.wrap(delete_query.getBytes()), Compression.NONE);
	}
	
	public CqlResult  cql_getALL() throws InvalidRequestException, UnavailableException, TimedOutException, SchemaDisagreementException, TException {
		final String query = "SELECT * FROM %s;";
		final String select_query = String.format(query, columnFamily);
		CqlResult result = client.execute_cql_query(ByteBuffer.wrap(select_query.getBytes()), Compression.NONE);
		return result;
	}
	
	public CqlResult cql_get(final int KEY) throws InvalidRequestException, UnavailableException, TimedOutException, SchemaDisagreementException, TException {
		final String select_query = String.format(" SELECT * FROM %s.%s WHERE KEY=%d;",keyspace,columnFamily,KEY);
		return client.execute_cql_query(ByteBuffer.wrap(select_query.getBytes()),Compression.NONE);
	}
	
	/****************************
	 * GETTERS and SETTERS ******
	 ****************************/
	
	
	/**
	 * @return the consistencyLevel
	 */
	public ConsistencyLevel getConsistencyLevel() {
		return consistencyLevel;
	}
	/**
	 * @return the columnFamily
	 */
	public String getColumnFamily() {
		return columnFamily;
	}
	/**
	 * @param columnFamily the columnFamily to set
	 */
	public void setColumnFamily(String columnFamily) {
		this.columnFamily = columnFamily;
	}
	/**
	 * @return the keyspace
	 */
	public String getKeyspace() {
		return keyspace;
	}
	/**
	 * @param keyspace the keyspace to set
	 * @throws TException 
	 * @throws InvalidRequestException 
	 */
	public void setKeyspace(String keyspace) throws InvalidRequestException, TException {
		this.keyspace = keyspace;
		this.client.set_keyspace(this.keyspace);
	}
	/**
	 * @param consistencyLevel the consistencyLevel to set
	 */
	public void setConsistencyLevel(ConsistencyLevel consistencyLevel) {
		this.consistencyLevel = consistencyLevel;
	}
	/**
	 * @return the transport
	 */
	public TTransport getTransport() {
		return transport;
	}
	/**
	 * @return the protocol
	 */
	public TProtocol getProtocol() {
		return protocol;
	}
	/**
	 * @return the client
	 */
	public Cassandra.Client getClient() {
		return client;
	}
}
