package jobs;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import models.RTBGraphEntity;
import models.RTBGraphSeries;

import play.jobs.*;
import play.libs.F.Promise;

/**
 * 
 * @author gautam
 * Retrieve AnalyticsData from the Cassandra Database.
 *
 */
@Every("1mn")
public class CassandraRandomData extends Job<List> {
	
	public RTBGraphEntity getRandomEntity() {
		Random random = new Random();
		RTBGraphEntity entity = new RTBGraphEntity(random.nextInt(50), random.nextFloat()+random.nextInt(10));
		return entity;
	}
	public ArrayList<RTBGraphEntity> getRandomEntities(int size){
		ArrayList<RTBGraphEntity> entities = new ArrayList<RTBGraphEntity>();
		for(int i=0;i<size;i++) {
			entities.add(getRandomEntity());
		}
		return entities;
	}
	
	@Override
	public ArrayList<RTBGraphSeries> doJobWithResult(){
		ArrayList<RTBGraphSeries> series = new ArrayList<RTBGraphSeries>();		
		series.add(new RTBGraphSeries(getRandomEntities(50), "total") );
		series.add(new RTBGraphSeries(getRandomEntities(50), "won") );
		series.add(new RTBGraphSeries(getRandomEntities(50), "lost") );
		return series;
	}

}
