package jobs;

import gateway.cassandra.CassandraClient;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import org.apache.cassandra.thrift.InvalidRequestException;
import org.apache.cassandra.thrift.SchemaDisagreementException;
import org.apache.cassandra.thrift.TimedOutException;
import org.apache.cassandra.thrift.UnavailableException;
import org.apache.thrift.TException;

import play.jobs.Job;
import models.*;

public class CassandraData extends Job<ArrayList<RTBGraphSeries>> {
	@SuppressWarnings("finally")
	public ArrayList<RTBGraphSeries> doJobWithResult(){
		ArrayList<RTBGraphSeries> series = new ArrayList<RTBGraphSeries>();
		CassandraClient client;
		try {
			client = new CassandraClient("komlimobile");
			
			
			client.setColumnFamily("total");
	    	RTBGraphSeries total =RTBGraphSeries.RTBGraphSeriesFactory(client.cql_getALL(),"total");
	    	series.add(total);
	    	
	    	client.setColumnFamily("won");
	    	RTBGraphSeries won =RTBGraphSeries.RTBGraphSeriesFactory(client.cql_getALL(),"won");
	    	series.add(won);
	    	
	    	client.setColumnFamily("lost");
	    	RTBGraphSeries lost=RTBGraphSeries.RTBGraphSeriesFactory(client.cql_getALL(),"lost");
	    	series.add(lost);
	    	
	    	client.close();
	    	
		} catch (InvalidRequestException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnavailableException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TimedOutException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SchemaDisagreementException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			return series;
		}
		
	}
}
