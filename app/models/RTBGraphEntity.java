package models;

import play.*;
import play.db.jpa.*;

import javax.persistence.*;
import java.util.*;


public class RTBGraphEntity  {
    public final int KEY;
    public final float VALUE;
    
	public RTBGraphEntity(final int KEY,final float VALUE) {
		this.KEY=KEY;
		this.VALUE=VALUE;
	}
	
	public String  toString() {
		return "KEY:"+KEY+",VALUE"+VALUE;
	}
    
}
