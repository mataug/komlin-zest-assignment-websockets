package models;

import java.util.ArrayList;

public class ChartData {
	public final ArrayList<Float> total,won,lost;

	public ChartData(ArrayList<Float> total, ArrayList<Float> won,
			ArrayList<Float> lost) {
		super();
		this.total = total;
		this.won = won;
		this.lost = lost;
	}
	
}
