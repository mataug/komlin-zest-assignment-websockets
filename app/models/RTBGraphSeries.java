package models;

import play.*;
import play.db.jpa.*;

import javax.persistence.*;

import org.apache.cassandra.thrift.Column;
import org.apache.cassandra.thrift.CqlResult;
import org.apache.cassandra.thrift.CqlRow;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.util.*;


public class RTBGraphSeries {
    public final ArrayList<RTBGraphEntity> entities;
    public final String name;
    
	public RTBGraphSeries(ArrayList<RTBGraphEntity> entities, String name) {
		super();
		this.entities = entities;
		this.name = name;
	}
	/**
	 * 
	 * @return an Array list of values of all the entities
	 */
	public ArrayList<Float> getEntityArray(){
		ArrayList<Float> entities = new ArrayList<Float>();
		for(RTBGraphEntity entity : this.entities ) {
			entities.add(entity.VALUE);
		}
		return entities;
	}
	public static RTBGraphSeries RTBGraphSeriesFactory(CqlResult result,String name) throws UnsupportedEncodingException {
		
		RTBGraphSeries series = new RTBGraphSeries(new ArrayList<RTBGraphEntity>(), name);
		for(CqlRow row : result.getRows()) {
			int KEY=0;
			float VALUE=0.0f;
			for(Column columns : row.getColumns()) {
				if(new String(columns.getName(),"utf8").equals("KEY")) {
					KEY = ByteBuffer.wrap(columns.getValue()).getInt();
				}
				if(new String(columns.getName(),"utf8").equals("VALUE")) {
					VALUE = ByteBuffer.wrap(columns.getValue()).getFloat();
				}
			}
			series.entities.add(new RTBGraphEntity(KEY, VALUE));
		}
		return series;
	}
    
}
