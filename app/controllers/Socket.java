package controllers;

import static play.libs.F.Matcher.ClassOf;
import static play.libs.F.Matcher.Equals;
import static play.mvc.Http.WebSocketEvent.TextFrame;
import gateway.cassandra.CassandraClient;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import jobs.CassandraData;
import jobs.CassandraRandomData;
import models.ChartData;
import models.RTBGraphSeries;

import org.apache.cassandra.thrift.InvalidRequestException;
import org.apache.cassandra.thrift.SchemaDisagreementException;
import org.apache.cassandra.thrift.TimedOutException;
import org.apache.cassandra.thrift.UnavailableException;
import org.apache.thrift.TException;
import org.apache.thrift.transport.TTransportException;

import play.libs.F.Either;
import play.libs.F.Promise;
import play.mvc.Controller;
import play.mvc.Http.WebSocketEvent;
import play.mvc.WebSocketController;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class Socket extends Controller {
	
	public static void rebuildCassandraData() throws InvalidRequestException, TException, UnavailableException, TimedOutException, SchemaDisagreementException {
		CassandraClient client = new CassandraClient("komlimobile");
		int key;
		client.setColumnFamily("total");
		double [] total = {
				2.3, 3.1, 6.3, 7.2, 2.4, 8.7, 5.5, 2.1, 8.6, 7.4, 6.9, 7.1,
                7.9, 7.9, 7.5, 6.7, 7.7, 7.7, 7.4, 7.0, 7.1, 5.8, 5.9, 7.4,
                8.2, 9.5, 9.4, 8.1, 10.9, 10.4, 10.9, 12.4, 12.1, 9.5, 7.5,
                7.1, 4.5, 6.1, 7.8, 5.4, 2.1, 1.9, 4.8, 2.1, 6.3, 4.4, 4.2,
                3.0, 3.0
		};
		key=0;
		for(double t:total) {
			client.cql_insert(key++, (float) t );
		}
		
		client.setColumnFamily("won");
		double [] won = {
				4.3, 5.1, 4.3, 5.2, 5.4, 4.7, 3.5, 4.1, 5.6, 7.4, 6.9, 7.1,
                4.9, 5.9, 3.5, 3.7, 4.7, 5.7, 3.4, 5.0, 2.1, 7.8, 2.9, 5.4,
                9.2, 5.5, 4.4, 5.1, 4.9, 6.4, 7.9, 8.4, 12.1, 9.5, 7.5,
                7.1, 7.5, 8.1, 6.8, 3.4, 2.1, 1.9, 2.8, 2.9, 1.3, 4.4, 4.2,
                3.0, 3.0
		};
		key=0;
		for(double w:won) {
			client.cql_insert(key++, (float) w);
		}
		
		client.setColumnFamily("lost");
		double [] lost = {
				0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.1, 0.0, 0.3, 0.0,
                0.0, 0.4, 0.0, 0.1, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
                0.0, 0.6, 1.2, 1.7, 0.7, 2.9, 4.1, 2.6, 3.7, 3.9, 1.7, 2.3,
                3.0, 3.3, 4.8, 5.0, 4.8, 5.0, 3.2, 2.0, 0.9, 0.4, 0.3, 0.5, 0.4
		};
		key=0;
		for(double l:lost) {
			client.cql_insert(key++, (float) l);
		}		
		client.close();
		renderText("Cassandra data Rebuilt");
	}
	
    public static void index(boolean random) throws InvalidRequestException, TException, UnsupportedEncodingException, UnavailableException, TimedOutException, SchemaDisagreementException {
    	CassandraClient client = new CassandraClient("komlimobile");
    	
    	client.setColumnFamily("total");
    	RTBGraphSeries total =RTBGraphSeries.RTBGraphSeriesFactory(client.cql_getALL(),"total");
    	
    	client.setColumnFamily("won");
    	RTBGraphSeries won =RTBGraphSeries.RTBGraphSeriesFactory(client.cql_getALL(),"won");
    	
    	client.setColumnFamily("lost");
    	RTBGraphSeries lost=RTBGraphSeries.RTBGraphSeriesFactory(client.cql_getALL(),"lost");
    	
    	client.close();
    	ChartData data = new ChartData(total.getEntityArray(), won.getEntityArray(), lost.getEntityArray());
    	if(random) {
    		renderArgs.put("random", true);
    	}
    	renderArgs.put("ChartData",data);
        render();
    }
    
   
    public static class WebSocket extends WebSocketController{
    	public static void open() throws InvalidRequestException, TException {
    		CassandraClient client = new CassandraClient("komlimobile");
    		CassandraData cassandraData = new CassandraData();
    		while(inbound.isOpen()) {
    			Either<WebSocketEvent,ArrayList<RTBGraphSeries>> event = await(Promise.waitEither(
    					inbound.nextEvent(),
    					cassandraData.in(10)
				));
    			
    			for(String signal:TextFrame.and(Equals("close")).match(event._1)) {
    				disconnect();
    			}
    			
    			for(ArrayList<RTBGraphSeries> series :ClassOf(ArrayList.class).match(event._2)) {
    				ChartData data = new ChartData(
    						series.get(0).getEntityArray(), 
    						series.get(1).getEntityArray(), 
    						series.get(2).getEntityArray()
    				);
    				outbound.send(new Gson().toJson(data));
    			}
    		}
    		client.close();
    	}
    	public static void random(){
    		CassandraRandomData cassandraData = new CassandraRandomData();
    		while(inbound.isOpen()) {
    			Either<WebSocketEvent, List> event = await(Promise.waitEither(
    					inbound.nextEvent(),
    					cassandraData.in(10)
				));
    			
    			for(String signal:TextFrame.and(Equals("close")).match(event._1)) {
    				disconnect();
    			}
    			
    			for(ArrayList<RTBGraphSeries> series :ClassOf(ArrayList.class).match(event._2)) {
    				ChartData data = new ChartData(
    						series.get(0).getEntityArray(), 
    						series.get(1).getEntityArray(), 
    						series.get(2).getEntityArray()
    				);
    				outbound.send(new Gson().toJson(data));
    			}
    		}
    	}
    }
}
