package controllers;

import play.*;
import play.mvc.*;

import gateway.cassandra.CassandraClient;

import java.io.UnsupportedEncodingException;
import java.util.*;

import org.apache.cassandra.thrift.InvalidRequestException;
import org.apache.cassandra.thrift.SchemaDisagreementException;
import org.apache.cassandra.thrift.TimedOutException;
import org.apache.cassandra.thrift.UnavailableException;
import org.apache.thrift.TException;
import org.apache.thrift.transport.TTransportException;

import models.*;

public class Admin extends Controller
{
    public static void index()
    {
        render();
    }
    public static void getAll(final String columnFamily) throws UnsupportedEncodingException, InvalidRequestException, UnavailableException, TimedOutException, SchemaDisagreementException, TException {
    	final CassandraClient client = new CassandraClient("komlimobile",columnFamily);
    	RTBGraphSeries series = RTBGraphSeries.RTBGraphSeriesFactory(client.cql_getALL(), columnFamily);
    	client.close();
    	renderJSON(series);
    }
    
    public static void get(final String columnFamily,int KEY) throws UnsupportedEncodingException, InvalidRequestException, UnavailableException, TimedOutException, SchemaDisagreementException, TException {
    	final CassandraClient client = new CassandraClient("komlimobile",columnFamily);
    	RTBGraphSeries series = RTBGraphSeries.RTBGraphSeriesFactory(client.cql_get(KEY), columnFamily);
    	client.close();
    	renderJSON(series);
    }
    
    public static void update(final String columnFamily,final int KEY,final float VALUE) throws InvalidRequestException, TException, UnavailableException, TimedOutException, SchemaDisagreementException {
    	final CassandraClient client = new CassandraClient("komlimobile",columnFamily);
    	client.cql_update(KEY, VALUE);
    	client.close();
    	renderJSON("{status:'success'}");
    }
    
    public static void insert(final String columnFamily,final int KEY,final float VALUE) throws InvalidRequestException, TException, UnavailableException, TimedOutException, SchemaDisagreementException {
    	final CassandraClient client = new CassandraClient("komlimobile",columnFamily);
    	client.cql_update(KEY, VALUE);
    	client.close();
    	renderJSON("{status:'success'}");
    }
    
    public static void delete(final String columnFamily,final int KEY) throws InvalidRequestException, TException, UnavailableException, TimedOutException, SchemaDisagreementException {
    	final CassandraClient client = new CassandraClient("komlimobile",columnFamily);
    	client.cql_delete(KEY);
    	client.close();
    	renderJSON("{status:'success'}");
    }
    
    
}
